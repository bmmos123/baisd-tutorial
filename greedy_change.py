def change(amount):

    one_baht = 0
    two_baht = 0
    four_baht = 0
    five_baht = 0
    ten_baht = 0

    while amount != 0:
        if amount >= 10:
            amount -= 10
            ten_baht += 1

        elif amount >= 5:
            amount -= 5
            five_baht += 1

        elif amount >= 4:
            amount -= 4
            four_baht += 1

        elif amount >= 2:
            amount -= 2
            two_baht += 1

        elif amount >= 1:
            amount -= 1
            one_baht += 1

    return f"10 baht = {ten_baht} coin\n5 baht = {five_baht} coin\n4 baht = {four_baht} coin\n2 baht = {two_baht} coin\n1 baht = {one_baht} coin"


if __name__ == "__main__":
    amount = int(input("Enter change:"))
    print(change(amount))
