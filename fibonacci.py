import timeit


def find_fibonacci(number):
    if number in [0, 1]:
        return number
    return find_fibonacci(number - 1) + find_fibonacci(number - 2)


def fibonacci_bottom_up(number):
    f0 = 0
    f1 = 1
    f2 = 2
    for i in range(2, number + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    print(
        "recursive fibonacci: ",
        find_fibonacci(10),
        timeit.timeit("find_fibonacci(10)", globals=globals()),
    )
    print(
        "b2u fibonacci: ",
        fibonacci_bottom_up(10),
        timeit.timeit("fibonacci_bottom_up(10)", globals=globals()),
    )
